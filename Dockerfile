FROM drentech/symfony

RUN yum install -y php-pgsql php-pdo

RUN mkdir -p /var/www/innihald

COPY . /var/www/innihald

WORKDIR /var/www/innihald
EXPOSE 9001
CMD php-fpm -F