<?php

namespace App\Entity;

use App\Entity\Traits\SoftDeletable;
use App\Entity\Traits\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PhysicalFileRepository")
 */
class PhysicalFile
{
    use SoftDeletable;
    use Timestampable;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $filename;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private string $extension;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private ?bool $hasThumbnail;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Document", mappedBy="physicalFile")
     */
    private Collection $documents;

    public function __construct()
    {
        $this->documents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getExtension(): ?string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): self
    {
        $this->extension = $extension;

        return $this;
    }

    public function getHasThumbnail(): ?bool
    {
        return $this->hasThumbnail;
    }

    public function setHasThumbnail(?bool $hasThumbnail): self
    {
        $this->hasThumbnail = $hasThumbnail;

        return $this;
    }

    /**
     * @return Collection|Document[]
     */
    public function getDocuments(): Collection
    {
        return $this->documents;
    }

    public function addDocument(Document $document): self
    {
        if (!$this->documents->contains($document)) {
            $this->documents[] = $document;
            $document->setPhysicalFile($this);
        }

        return $this;
    }

    public function removeDocument(Document $document): self
    {
        if ($this->documents->contains($document)) {
            $this->documents->removeElement($document);
            // set the owning side to null (unless already changed)
            if ($document->getPhysicalFile() === $this) {
                $document->setPhysicalFile(null);
            }
        }

        return $this;
    }
}
