<?php

namespace App\Repository;

use App\Entity\DocumentContent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DocumentContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentContent[]    findAll()
 * @method DocumentContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentContentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentContent::class);
    }

    // /**
    //  * @return DocumentContent[] Returns an array of DocumentContent objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentContent
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
