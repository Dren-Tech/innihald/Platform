<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * NewDocumentMessage.php of project Innihald.
 * Created by user marian at 16.02.20.
 */

namespace App\Message;


class NewDocumentMessage implements AsyncMessage
{

    public function getName(): string
    {
        // TODO: Implement getName() method.
    }
}