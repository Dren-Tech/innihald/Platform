<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * ImportFileHandler.php of project Innihald.
 * Created by user marian at 26.01.20.
 */

namespace App\MessageHandler;


use App\Entity\Document;
use App\Entity\PhysicalFile;
use App\Message\ImportFileMessage;
use App\Service\Entity\DocumentService;
use App\Service\Entity\PhysicalFileService;
use App\Service\FilenameService;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\MountManager;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ImportFileHandler implements MessageHandlerInterface
{
    private DocumentService $documentService;

    private PhysicalFileService $fileService;

    private MountManager $mountManager;

    private MessageBusInterface $bus;

    private FilenameService $filenameService;

    /**
     * ImportFileHandler constructor.
     * @param DocumentService $documentService
     * @param PhysicalFileService $fileService
     * @param MessageBusInterface $bus
     * @param FilesystemInterface $tempStorage
     * @param FilesystemInterface $repositoryStorage
     * @param FilenameService $filenameService
     */
    public function __construct(DocumentService $documentService, PhysicalFileService $fileService, MessageBusInterface $bus, FilesystemInterface $tempStorage, FilesystemInterface $repositoryStorage, FilenameService $filenameService)
    {
        $this->documentService = $documentService;
        $this->fileService = $fileService;
        $this->bus = $bus;

        $this->mountManager = new MountManager([
            'temp' => $tempStorage,
            'repo' => $repositoryStorage
        ]);

        $this->filenameService = $filenameService;
    }


    public function __invoke(ImportFileMessage $message)
    {
        //TODO: move this to own service
        $newFilename = $this->filenameService->getFilenameForFile($message->getFilename());
        $newFolder = $this->filenameService->getFoldernameForFile($newFilename);

        $this->mountManager->move('temp://import/' . $message->getFilename(),
            sprintf("repo://%s/%s.%s", $newFolder, $newFilename, $message->getExtension()));


        //TODO: move this to own service
        $file = new PhysicalFile();
        $file->setFilename($newFilename);
        $file->setExtension($message->getExtension());
        $file->setHasThumbnail(false);

        $this->fileService->saveFile($file);

        $document = new Document();
        $document->setTitle(explode(".", $message->getFilename())[0]);
        $document->setDescription("Automatic import");
        $document->setPhysicalFile($file);

        $this->documentService->saveDocument($document);

        //TODO: create new message that new document is saved -> generate pdf, do OCR/content extraction
    }

}