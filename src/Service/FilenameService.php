<?php
declare(strict_types=1);

/**
 * Copyright (c) 2020 Marian Hahne
 * Licensed under MIT license. See LICENSE.md for more information.
 *
 * FilenameService.php of project Innihald.
 * Created by user marian at 02.02.20.
 */

namespace App\Service;


class FilenameService
{
    public function getFilenameForFile(string $file): string {
        return hash('sha256', $file);
    }

    public function getFoldernameForFile(string $file): string {
        return substr($file, 0, 2);
    }
}