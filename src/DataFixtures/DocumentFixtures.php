<?php

namespace App\DataFixtures;

use App\Entity\Document;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class DocumentFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= 10; $i++) {
            $doc = new Document();
            $doc->setTitle("Document " . $i);
            $doc->setDescription("Description " . $i);

            $manager->persist($doc);
        }

        $manager->flush();
    }
}
