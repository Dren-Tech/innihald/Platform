<?php

namespace App\Command;

use App\Message\ImportFileMessage;
use League\Flysystem\FileExistsException;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemInterface;
use League\Flysystem\MountManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class ImportFolderCommand extends Command
{
    protected static $defaultName = 'import:folder';

    private static array $blacklistedExtensions = [
        'xml', 'xsl', 'exe', 'toml'
    ];

    private ParameterBagInterface $params;

    private FilesystemInterface $importStorage;

    private FilesystemInterface $tempStorage;

    private MountManager $mountManager;

    private MessageBusInterface $bus;

    /**
     * ImportFolderCommand constructor.
     * @param ParameterBagInterface $params
     * @param FilesystemInterface $importStorage
     * @param FilesystemInterface $tempStorage
     * @param MessageBusInterface $bus
     */
    public function __construct(ParameterBagInterface $params, FilesystemInterface $importStorage, FilesystemInterface $tempStorage, MessageBusInterface $bus)
    {
        parent::__construct($this::$defaultName);

        $this->params = $params;
        $this->importStorage = $importStorage;
        $this->importStorage = $importStorage;
        $this->bus = $bus;

        $this->mountManager = new MountManager([
            'import' => $importStorage,
            'temp' => $tempStorage
        ]);
    }


    protected function configure()
    {
        $this
            ->setDescription('Import new documents from folder')
            //->addArgument('folder', InputArgument::OPTIONAL, 'Folder from which the documents should be imported')
            ->addOption('recursive', null, InputOption::VALUE_NONE, 'Should the folder be imported recursively?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $folderContent = $this->mountManager->listContents("import://", $input->getOption('recursive'));

        foreach ($folderContent as $file) {
            //TODO: move blacklisted extensions to own config
            if($file['type'] == 'file' && !in_array($file['extension'], $this::$blacklistedExtensions)) {
                // move files to temp import folder
                $this->mountManager->move('import://' . $file['path'], 'temp://import/' . $file['path']);

                // send message to import new file
                $this->bus
                    ->dispatch(new ImportFileMessage($file['basename'], $file['extension']));
            }
        }

        $io->success(sprintf('Successful import %s documents.', count($folderContent)));

        return 0;
    }
}
